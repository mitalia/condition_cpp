#include "condition.h"

#include <stdexcept>
#include <typeinfo>
#include <iostream>

struct ArithmeticError : Condition { };
struct DivByZero : ArithmeticError { double replacementValue; };

double divide(double num, double den) {
    if(den == 0.) {
        DivByZero cond;
        if(!signal_condition(cond)) {
            throw std::invalid_argument("division by zero");
        }
        return cond.replacementValue;
    }
    return num/den;
}

void bar() {
    std::cout<<divide(10, 0.)<<"\n";
}

void foo() {
    Handler h(cond_filter<ArithmeticError>([] (ArithmeticError *cond){
        std::cerr<<"There's an ArithmeticError passing here! "<<typeid(*cond).name()<<"\n";
        return false;
    }));
    bar();
}

int main() {
    {
        Handler h(cond_filter<DivByZero>([] (DivByZero *cond) {
            cond->replacementValue = 15;
            return true;
        }));
        bar();
        std::cerr<<"---\n";
        foo();
        std::cerr<<"---\n";
    }
    bar();
}

