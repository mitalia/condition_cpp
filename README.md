# Conditions System for C++ - vaguely inspired by Common Lisp #

Exceptions allow a function to signal an error condition to callers that
installed handlers for it; however, once an exception is thrown the stack
gets unwound up to the exception handler, which generally cannot do any
corrective action.

With conditions, instead, the handler gets called *by the code which is
having trouble*, and can potentially provide some corrective action. For
example, if some code cannot open a file, instead of raising an exception
it can signal the condition, and the handler can e.g. provide an alternative
file name to try.

## Usage ##

In this library, a condition is just an object (inheriting from `Condition`)
that is dispatched to the first handler that chooses to handle it, and is
used as a way for it to communicate back to the faulting code how to fix
the problem.

```cpp
struct ArithmeticError : Condition { };
struct DivByZero : ArithmeticError { double replacementValue; };
```

The faulting code constructs the condition and signals it using
`signal_condition`; if `signal_condition` returns `false`, it means that no
handler accepted it.

```cpp
double divide(double num, double den) {
    if(den == 0.) {
        DivByZero cond;
        if(!signal_condition(cond)) {
            throw std::invalid_argument("division by zero");
        }
        return cond.replacementValue;
    }
    return num/den;
}
```

When a condition is signaled, the (thread-local) chain of handlers is
walked, and each one is invoked passing the condition object until one of
them handles it (=the handler function returns `true`) is found.

To define a handler, a `Handler` object must be instantiated *on the stack*,
passing a `bool(Condition *)` callable; the handler is active as long as the
object is alive.

To help with the common case of filtering by condition type, `cond_filter` is
provided; it performs a `dynamic_cast` to the desired type and calls the
given handler only if it succeeds.

```cpp
Handler h(cond_filter<DivByZero>([] (DivByZero *cond) {
    cond->replacementValue = 15;
    return true;
}));
```

A handler may be installed even just to look at the passing conditions,
without ever handling anything.

```cpp
Handler h(cond_filter<ArithmeticError>([] (ArithmeticError *cond){
    std::cerr<<"There's an ArithmeticError passing here! "<<typeid(*cond).name()<<"\n";
    return false;
}));
```

## Notes ##

Common Lisp conditions provide *restart points* to the handler, which are
functions defined locally to the code signaling the condition, and are
provided to the handler as possible corrective actions.

In this implementation, due to limitations of C++ and reduced scope we aim
for, we just limit ourselves to transmitting the condition object to the
handlers; anything else is left to be implemented to the library user.
