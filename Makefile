all: cond_test

cond_test: cond_test.cpp condition.h
	g++ -O3 -std=c++11 cond_test.cpp -ocond_test.x

clean:
	rm -f cond_test.x
