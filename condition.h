#ifndef CONDITION_CPP_CONDITION_H_INCLUDED
#define CONDITION_CPP_CONDITION_H_INCLUDED
#include <functional>

/// For rationale and usage, see README.md

/// All conditions must derive from Condition; this is created by the faulting
/// code, passed to the handlers which can modify it to provide a corrective
/// action.
struct Condition {
    virtual ~Condition() {};
};

/// Installs a handler function in the handlers chain, removes it when gets
/// deleted. *Must be allocated on the stack*
struct Handler {
    /// Handler function type; takes the condition, returns true if it was handled
    typedef std::function<bool(Condition *)> FnT;
    FnT fn; ///< Handler function
    Handler *prev = nullptr;    ///< previous node in the handlers chain

    /// Head of the handlers list
    static Handler *&head() {
        thread_local Handler *h = nullptr;
        return h;
    }

    Handler(FnT fn) : fn(fn) {
        prev = head();
        head() = this;
    }

    Handler(const Handler&)=delete;

    virtual ~Handler() {
        head() = prev;
    }
};

/// Helper to get a handler function that automatically filters by type
template<typename CondT, typename FnT>
Handler::FnT cond_filter(FnT fn) {
    return [fn](Condition *c) -> bool {
        if(auto cc = dynamic_cast<CondT *>(c)) {
            return fn(cc);
        }
        return false;
    };
}

/// Signals the given condition, passing it to the handlers; if some handler
/// actually handled it, returns true, false otherwise.
bool signal_condition(Condition &cond) {
    Handler *hb = Handler::head();
    while(hb) {
        if(hb->fn(&cond)) return true;
        hb = hb->prev;
    }
    return false;
}
#endif
